LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := ti_signal_rild.c
LOCAL_MODULE := ti_signal_rild
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
	libc

include $(BUILD_EXECUTABLE)
