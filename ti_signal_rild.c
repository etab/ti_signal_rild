

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#define LOG_TAG "RIL"
#define LOG_NDEBUG 1
#include <utils/Log.h>

#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <linux/netlink.h>

#include <signal.h>
#include <unistd.h>

#define DEBUG_UEVENT 1
#define UEVENT_PARAMS_MAX 32

#define PATH_SIZE 1024
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))

/*enum {
     HUAWEI_MODEM = 0,
     };*/


enum uevent_action { action_add, action_remove, action_change };


/*uvent structure*/
struct uevent {
	char *path;
	enum uevent_action action;
	char *subsystem;
	char *param[UEVENT_PARAMS_MAX];
	unsigned int seqnum;
};


void process_uevent_message(int sock)
{        
	LOGE(" inside process uvent\n");
	LOGE("---thread process_uevent_messade callded");
	char buffer[64 * 1024];
	char vbuf[5], pbuf[5];

	int i;
	char *s = buffer, *p;
	char *end;
	int count, param_idx = 0, ret;
	struct uevent *event;
	count = recv(sock, buffer, sizeof(buffer), 0); /* when inserted the modem uvent comes and info is stored in the buffer*/

	LOGE(" size of buffer isss  %d\n", sizeof(buffer));
	LOGE(" Count is %d\n", count);

	/*    for(i=0;i<count;i++)
	      { 
	      LOGE("%c %d", buffer[i],i);
	      }
	      LOGE( " buffer is %s\n",buffer);*/

	if (count < 0) {
		LOGE("Error receiving uevent (%s)", strerror(errno));
		return -errno;
	}
	event = malloc(sizeof(struct uevent));
	if (!event) { 
		LOGE("Error allcating memroy (%s)", strerror(errno));
		return -errno;
	}
	memset(event, 0, sizeof(struct uevent));

	end = s + count;

	for (p = s; *p != '@'; p++)
		;
	p++;
	event->path = strdup(p);
	s += strlen(s) + 1;
	LOGE( " s is printed as %s \n",s );
	while (s < end) {
		/* comapring the buffer for ACTION add or remove or change state */
		if (!strncmp(s, "ACTION=", strlen("ACTION="))) {
			char *a = s + strlen("ACTION=");
			char action[25];
			memcpy(&action, a, 6);
			LOGE(" action issss %s \n", action);
			if (!strcmp(a, "add"))
				event->action = action_add;

			else if (!strcmp(a, "change"))
				event->action = action_change;
			else if (!strcmp(a, "remove"))
				event->action = action_remove;
		} else if (!strncmp(s, "SEQNUM=", strlen("SEQNUM=")))
			event->seqnum = atoi(s + strlen("SEQNUM="));
		/* Comparing for Product and if its equal to 12d1 and action is add start the rild else action is remove stop the rild */

		else if (!strncmp(s, "PRODUCT=", strlen("PRODUCT="))){       
			LOGE("inside elseif \n");

			char *t = s + strlen("PRODUCT="); 

			LOGE(" product id  is %s \n", t);
			for(i=0; i<5; i++)
				LOGE("%c\t",*(t+i));

			LOGE("ENteringifelse\n");				
			if ((!strncmp(t, "12d1", 4)) && (event->action == action_add))
			{  
                                LOGE("inside huewai:airtel \n");
                                property_set("ril.gsm.modemtype" ,"HUAWEI");        
				property_set("ril.gsm.deviceport", "/dev/ttyUSB3");
				property_set("ril.gsm.dataport", "/dev/ttyUSB0");
                                property_set("ctl.start","ril-daemon");
				LOGE("AFTER propertySET\n");
                             
                                
			}
                       else if ((!strncmp(t, "230d", 4)) && (event->action == action_add))
			{  
                                LOGE("inside teracom:bsnl \n");
                                property_set("ril.gsm.modemtype" ,"TERACOM");        
				property_set("ril.gsm.deviceport", "/dev/ttyACM1");
				//property_set("ril.gsm.dataport", "/dev/ttyACM1");
                                property_set("ctl.start","ril-daemon");
				LOGE("AFTER propertySET\n");
                             
                                
			}

			else if((!strncmp(t, "12d1", 4)) && (event->action == action_remove))
			{
				LOGE("inside remove\n");
				property_set("ctl.stop","ril-daemon"); 
                               
			}
                        else if((!strncmp(t, "230d", 4)) && (event->action == action_remove))
			{
				LOGE("inside teracom remove\n");
				property_set("ctl.stop","ril-daemon"); 
                               
			}


		}
		else
			event->param[param_idx++] = strdup(s);
		s += strlen(s) + 1;
	}


}


int main()
{
	/* uvent handling */
        property_set("ctl.stop","ril-daemon");
	struct sockaddr_nl nladdr;
	struct pollfd pollfds[2];
	int uevent_sock;
	int ret, max = 0;
	int uevent_sz = 64 * 1024;
	int timeout = -1;
	struct sigaction timeoutsigact;

	LOGE("3G modem monitor thread is start");
        //system("setprop ctl.stop ril-daemon");
        
	sigemptyset(&timeoutsigact.sa_mask);
	sigaddset(&timeoutsigact.sa_mask, SIGALRM);
	sigaction(SIGALRM, &timeoutsigact, 0);

	memset(&nladdr, 0, sizeof(nladdr));
	nladdr.nl_family = AF_NETLINK;
	nladdr.nl_pid = getpid();
	nladdr.nl_groups = 0xffffffff;

	uevent_sock = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
	if (uevent_sock < 0) {
		LOGE(" Netlink socket faild, usb monitor exiting...");
		return NULL;
	}

	if (setsockopt(uevent_sock, SOL_SOCKET, SO_RCVBUFFORCE, &uevent_sz,
				sizeof(uevent_sz)) < 0) {
		LOGE("Unable to set uevent socket options: %s", strerror(errno));
		return NULL;
	}

	if (bind(uevent_sock, (struct sockaddr *) &nladdr, sizeof(nladdr)) < 0) {
		LOGE("Unable to bind uevent socket: %s", strerror(errno));
		return NULL;
	}
	pollfds[0].fd = uevent_sock;
	pollfds[0].events = POLLIN;

	ret = fcntl(uevent_sock,F_SETFL, O_NONBLOCK);
	if (ret < 0)
		LOGE("Error on fcntl:%s", strerror(errno));

	while (1) {
		ret = poll(pollfds, 1, timeout);

		switch (ret) {
			case 0:
				LOGD("poll timeout");
				continue;
			case -1:
				LOGD("poll error:%s", strerror(errno));
				break;

			default:
				if (pollfds[0].revents & POLLIN)
					LOGE("goin into procees uevnt\n");
				process_uevent_message(uevent_sock); /* this will invoke process_uvent_message func */ 
				LOGE(" comin out\n");
		}
	}

	close(uevent_sock);
}

